package Matrix;

        import java.io.BufferedWriter;
        import java.io.FileWriter;
        import java.io.IOException;
        import java.util.ArrayList;
        import java.util.List;
        import java.util.Random;
        import java.util.concurrent.*;

        /**
         * Created by user on 09.11.2016.
         */
        public class MultiplyThreadMatrixExecutor {


            private static final int MATRIX_SIZE = 200;

            static int aMatrix[][] = new int[MATRIX_SIZE][MATRIX_SIZE];

            static int bMatrix[][] = new int[MATRIX_SIZE][MATRIX_SIZE];

            static int resultMatrixSize = aMatrix.length * bMatrix[0].length;

            static int resultMatrix[][] = new int[aMatrix.length][bMatrix[0].length];



            public static void main(String[] args) throws InterruptedException, IOException {


                fillRandom(aMatrix);

                fillRandom(bMatrix);


                List<Callable<int[][]>> tasks = createTasks(4);


                ExecutorService executorService4Thread = Executors.newFixedThreadPool(4);

                Long start = System.currentTimeMillis();

                List<Future<int[][]>> futures = executorService4Thread.invokeAll(tasks);

                resultMatrix = getResult(futures);

                executorService4Thread.shutdown();

                Long time = System.currentTimeMillis() - start;

                System.out.println("Parallel calculation (4) took: " + time + " milliseconds.");

                writeToFile(resultMatrix, "RezultMatrix(4).txt");


            }


            public static List<Callable<int[][]>> createTasks(int tasksCount) {

                List<Callable<int[][]>> tasks = new ArrayList<>();

                final int partSize = resultMatrixSize / tasksCount;

                for (int i = 0; i < tasksCount; i++) {

                    final int finalI = i;

                    Callable<int[][]> callable = new Callable<int[][]>() {

                        @Override

                        public int[][] call() throws Exception {

                            int from = finalI * partSize;

                            return sequentialCalculation(aMatrix, bMatrix, from, from + partSize);

                        }

                    };

                    tasks.add(callable);

                }

                return tasks;

            }

            private long[][] multiply(int from, int to) {
                long[][] resultMatrix = new long[to - from + 1][bMatrix[0].length];
                for (int i = from; i <= to; i++) {
                    for (int j = 0; j < aMatrix[0].length; j++) {
                        int n = 0;
                        for (int k = 0; k < bMatrix.length; k++) {
                            n += aMatrix[i][k] * bMatrix[k][j];
                        }
                        resultMatrix[i - from][j] = n;
                    }
                }
                return resultMatrix;
            }


            private static int[][] sequentialCalculation(int[][] aMatrix, int[][] bMatrix, int from, int to) {

                int rows = (to - from + 1) / aMatrix.length;

                int cols = aMatrix.length;


                int[][] res = new int[rows][cols];

                for (int i = from; i < to; i++) {

                    int row = i / aMatrix.length;

                    int col = i % bMatrix[0].length;

                    res[row - (from / aMatrix.length)][col] = calculateMultipleValue(getRow(row, aMatrix), getCol(col, bMatrix));

                }


                return res;

            }




            private static int[] getRow(int rowNumber, int[][] matrix) {

                int[] row = new int[matrix[0].length];

                System.arraycopy(matrix[rowNumber], 0, row, 0, matrix[0].length);

                return row;

            }


            private static int[] getCol(int colNumber, int[][] matrix) {

                int[] col = new int[matrix.length];

                for (int i = 0; i < matrix.length; i++)

                    col[i] = matrix[i][colNumber];

                return col;

            }


            private static int calculateMultipleValue(int[] row, int[] col) {

                int result = 0;


                for (int i = 0; i < row.length; i++)

                    result += row[i] * col[i];

                return result;

            }


            public static void fillRandom(int[][] matrix) {

                Random rnd = new Random();

                for (int i = 0; i < matrix.length; i++)

                    for (int j = 0; j < matrix[0].length; j++) {

                        matrix[i][j] = rnd.nextInt(9);

                    }

            }


            private static void writeToFile(int[][] matrix, String fileName) throws IOException {

                BufferedWriter fileWriter = new BufferedWriter(new FileWriter(fileName));

                for (int i = 0; i < matrix.length; i++) {

                    for (int j = 0; j < matrix[0].length; j++) {

                        fileWriter.write(matrix[i][j] + " ");

                    }

                    fileWriter.newLine();

                }

                fileWriter.close();

            }


            public static int[][] getResult(List<Future<int[][]>> futures) {

                try {

                    int[][] result = null;


                    for (int k = 0; k < futures.size(); k++) {

                        Future<int[][]> f = futures.get(k);

                        int[][] part = f.get();

                        if (result == null) {

                            result = new int[part.length * futures.size()][part[0].length];

                        }

                        for (int i = 0; i < part.length; i++) {

                            for (int j = 0; j < part[0].length; j++) {

                                result[i + part.length * k][j] = part[i][j];

                            }

                        }

                    }

                    return result;

                } catch (ExecutionException | InterruptedException e) {

                    e.printStackTrace();

                    return null;

                }

            }

        }










